pragma solidity ^0.5.16;

// THIS CONTRACT CONTAINS BUGS - DO NOT USE
contract Insecure01 {


   /// Mapping of ether shares of the contract.
   mapping(address => uint) public shares;
   address owner;
   address[] shareholders;
   event FailedSend(address, uint);

   constructor() public {
      owner = msg.sender;
   }

   function () payable external {
      shares[msg.sender] += msg.value;
   }

   /// Withdraw your share.
   function withdraw() public {
     if (msg.sender.send(shares[msg.sender])) {
         shares[msg.sender] = 0;
      } else {
         emit FailedSend(msg.sender, shares[msg.sender]);
      }
   }

}
